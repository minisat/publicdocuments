# Первый проект в STM32CubeIDE - Мигаем светодиодом

Урок также существует в видеоформате на [YouTube](https://youtu.be/Xe9DB_KNnDA?si=IN-Rf4KLBplJxuBS) и [RuTube](https://rutube.ru/video/4f0c07e72647015b67cbd1116007e87b/)

## Что потребуется
**Из комплекта MiniSat:**
- Плата MCU

**Дополнительно:**
- Компьютер / ноутбук с [STM32CubeIDE](STM32CubeIDESetup.md)
- [Программатор / отладчик](../../Equipment/Readme.md#программатор--отладчик)

Для того, чтобы создать новый проект, необходимо во вкладке `File` выбрать `New -> STM32 Project`. 

> Если после нажатия на кнопку выскочило сообщение вида ***"In order to benefit from best experience a network connection is required. Do you want to set connection parameter now?"***, то следует произвести [настройку среды](STM32CubeIDESetup.md#как-настроить).

В открывшемся окне необходимо указать, какой конкретно микроконтроллер нам предстоит программировать. Их у производителя достаточно много и тут важно не ошибиться, поскольку код, собранный под один микроконтроллер, не будет адекватно запускаться на другом, и результат может быть непредсказуемым.

![alt text](SourceImg/InstructionsScreenShots/TargetSelector.jpg "Target Selector")
<div align="center"> <b>Выбор микроконтроллера</b> </div>

 В поле `Part Number` можно указать название нужного микроконтроллера, чтобы сузить список, из которого предстоит выбирать. В конструкторе MiniSat (атмосферная версия) используется микроконтроллер **STM32F102C4** (версии бортового компьютера до 0.2 включительно) либо **STM32F103C8** (версия 0.3 и выше).  Если вбить это название, то в списке он будет всего один физический (буквы после отвечают за упаковку) - можно выбрать любой и нажимать `Next`.
 
> Можно также нажать на "звёздочку" в списке, и тогда данный микроконтроллер сохранится в список избранного, открывающийся по нажатию "звёздочки" над `Part Number`

![alt text](SourceImg/InstructionsScreenShots/TargetSelector_filtered.jpg "Target Selector с фильтрацией")
<div align="center"> <b>Выбор микроконтроллера - отфильтрованный список</b> </div>

В открывшемся окне необходимо назвать проект, после чего нажать `Finish`. При необходимости вместо `Finish` можно нажать `Next` и выбрать конкретную версию библиотек, на которых будет строиться проект. Необходимо в случаях, если есть конфликты версий библиотек или если они устанавливались вручную, а не автоматически через интернет.

После этого появится схематичное изображение микроконтроллера. Его теперь можно конфигурировать - выбирать нужные функции нужных выводов, выбирать режимы работы периферии. Но прежде, чем это делать, нужно обязательно провести очень важное действие. В меню слева открыть категорию `System Core` и выбрать `SYS`. На открывшейся панели управления выбрать в выпадающем списке около параметра `Debug` `Serial Wire`. **Если этого не сделать, то, после загрузки прошивки в микроконтроллер, интерфейс отладки будет выключен и залить новую прошивку через этот интерфейс будет невозможно (но всё ещё можно будет через другие интерфейсы).**

![alt text](SourceImg/InstructionsScreenShots/serial_wire.jpg "Настройка отладки")
<div align="center"> <b>Выбор отладочного интерфейса</b> </div>

Теперь мы хотим, чтобы наш микроконтроллер подал некоторые признаки жизни. Самый простой способ это сделать - поморгать встроенным в плату светодиодом. Для этого необходимо настроить на выход тот вывод, к которому он подключен. По умолчанию все выводы - входы и не дают напряжения. Напряжение на них определяется исключительно внешними источниками. Для настройки выводов щёлкаем по выбранной ножке, в данном случае `PA8`, левой кнопкой мыши и выбираем в выпавшем окне вариант `GPIO_Output`. Выбранная ножка должна загореться зелёным. Теперь в категории `System Core` можно открыть вкладку `GPIO` и в выпавшем списке выбрать нашу ножку, она будет единственная. После этого внизу окна появятся свойства ножки. В самом низу списка можно найти `User Label` и дать ножке название. Это облегчит работу с ней в последующем при написании кода. Можно не устанавливать своего названия, с ней все еще можно будет работать по стандартному имени. Как только ножке будет присвоено имя, оно тут же появится рядом с ней на изображении МК.

![alt text](SourceImg/InstructionsScreenShots/IOC_GPIO.jpg "Настройка порта")
<div align="center"> <b>Настройка порта на выход</b> </div>

Теперь необходимо написать код. Но прежде, чем мы начнём его писать, позволим CubeIDE провести некоторую подготовительную работу и написать часть кода за нас. Для этого необходимо на верхней панели управления нажать на иконку жёлтой шестерёнки (`Device Configuration Tool Code Generation`).

В отдельной вкладке откроется файл с кодом. Это стандартный код, внутри которого нужно написать код мигалки. При подаче питания на микроконтроллер программа начнется с первой строчки внутри функции `main(void)`. Команды отрабатываются одна за другой сверху вниз однократно. Для того чтобы какой-то алгоритм (например, мигание светодиода) работал непрерывно повторяясь, его нужно вписать в цикл. В стандартном коде уже есть заготовка цикла. Она имеет вид `while(1)`. Это буквально "бесконечный цикл". Для программы для микроконтроллера бесконечный цикл - обыкновенное явление, программа не должна заканчиваться, микроконтроллер всегда должен знать, что делать. Ничего не делать - тоже определенное действие.

![alt text](SourceImg/InstructionsScreenShots/Initial_Code.jpg "Сгенерированный код")
<div align="center"> <b>Сгенерированный пустой цикл</b> </div>

После открывающей фигурной скобки нужно вписать код. Светодиод по умолчанию выключен, поэтому первой командой необходимо его включить. Чтобы обратиться к ножке МК существует команда `HAL_GPIO_WritePin`. Чтобы найти, как правильно составить команду, нужно пролистать код и найти `static void MX_GPIO_Init(void)`. Внутри прописаны некоторые функции. Попробуйте найти там, какие аргументы использует функция `HAL_GPIO_WritePin`, и скопировать строчку в свой код два раза. В первой строчке заменить `RESET` на `SET`. Теперь, для того чтобы различать мигание, необходимо установить задержу. Это делается с помощью команды `HAL_Delay()`. В скобках пишется длительность задержки в миллисекундах. Для примера можно поставить 500. Задержки должно быть две. Теперь можно проверять код. Для этого на верхней панели управления нужно нажать на иконку зелёного жука (Debug) и код начнёт проверяться. Если ошибок не будет, откроется окно с настройкой процесса загрузки программы в микроконтроллер.

![alt text](SourceImg/InstructionsScreenShots/debug_configuration_1.jpg)
<div align="center"> <b>Первичная настройка отладки</b> </div>

В зависимости от того, каким устройством (отладчиком) происходит загрузка программы в микроконтроллер, нужно выбрать это устройство во вкладке Debugger. Если используется ST-Link, он должен быть выбран из списка. Если используется J-Link, необходимо выбрать его.

![alt text](SourceImg/InstructionsScreenShots/debug_configuration_2.jpg)
<div align="center"> <b>Выбор отладчика</b> </div>

Когда отладчик выбран, можно запускать отладку (`Debug`). Как только программа загрузится в память, выполнение остановится в самом начале. Его нужно запустить, нажав на верхней панели кнопку `Resume`. Должен начать мигать светодиод.

Таким образом, мы написали свою первую программу для пикоспутника MiniSat.
