# Учимся работать с датчиками и передавать их данные по радио

Данный урок существует в видеоформате

* [На Youtube](https://youtu.be/VDe-OfJhwVc)
* [Перезалив на Rutube](https://rutube.ru/video/e6af8553ebf9fec19f8d4a1574f20560/)

## Что потребуется
**Из комплекта MiniSat:**
- Плата MCU
- Плата Radio
- Плата EPS
- Плата Sensors
- Аккумулятор + кабель Micro USB для подзарядки
- Плата RCV
- [USB-UART адаптер](../../Equipment/Readme.md#usb-uart-адаптер)

**Дополнительно:**
- Компьютер / ноутбук с [STM32CubeIDE](STM32CubeIDESetup.md) и [терминалом COM-порта](../../Equipment/Readme.md#terminal-by-bry-%D0%B8%D0%BB%D0%B8-%D0%B0%D0%BD%D0%B0%D0%BB%D0%BE%D0%B3)
- [Программатор / отладчик](../../Equipment/Readme.md#программатор--отладчик)
- [Проводки мама-мама или папа-мама](../../Equipment/Readme.md#проводки)